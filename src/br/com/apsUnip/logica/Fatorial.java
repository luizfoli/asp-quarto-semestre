package br.com.apsUnip.logica;

/**
 * Classe repsons�vel por calcular o fatorial de um n�mero por recursividade.
 * 
 * @author LFO
 *
 */

public class Fatorial {

	/**
	 * Este � um m�todo est�tico que faz o calculo do fatorial de um n�mero que �
	 * passado como par�metro e retorna para a fun��o principal o resultado.
	 */
	public static int calcula(int num) {

		/**
		 * Este � o caso base, se o n�mero passado por parametro for 0 ou 1, ele retorna
		 * o resultado 1 e finaliza o m�todo.
		 */
		if (num <= 1) {

			return 1;

		} else {

			/**
			 * chama o m�todo fatorial novamente, mas dessa vez enviando como parametro (n -
			 * 1).
			 */

			return calcula(num - 1) * num;

		}

	}
}
