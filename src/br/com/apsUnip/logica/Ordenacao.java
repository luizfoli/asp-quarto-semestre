package br.com.apsUnip.logica;

import java.util.Arrays;

public class Ordenacao {

	public static String ordena(String abcd) {

		try {
			char[] chars = abcd.toCharArray();
			Arrays.sort(chars);
			String sorted = new String(chars);
			return sorted;
		} catch (Exception e) {
			return "Deu ruim!";
		}
	}

	public static long iniciaContarTempo() {
		return System.nanoTime();
	}

	public static long finalizaContarTempo() {
		return System.nanoTime();
	}

	public static long tempoExecucao(long inicio, long fim) {
		return (fim - inicio);
	}
}
