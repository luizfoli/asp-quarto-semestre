package br.com.apsUnip.gui;

import br.com.apsUnip.gui.telas.TelaInicial;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Interface extends Application {

	public static Stage frame = new Stage();
	private TelaInicial menu = new TelaInicial();
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		frame.getIcons().add(new Image(getClass().getResourceAsStream("logounip.png")));
		frame.setResizable(false);
		frame.setScene(menu.getSceneInicial());
		frame.setTitle("APS 4 Semestre - Ci�ncia da Computa��o");
		frame.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	
}
