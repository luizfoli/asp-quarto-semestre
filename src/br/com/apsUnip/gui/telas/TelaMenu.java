package br.com.apsUnip.gui.telas;

import br.com.apsUnip.gui.Interface;
import br.com.apsUnip.gui.elementos.Elementos;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class TelaMenu {

	private static AnchorPane telaMenu;
	private static Scene sceneMenu;

	private static Elementos elementos = new Elementos();

	private static TelaFatorial telaFatorial = new TelaFatorial();
	private static TelaOrdenacao telaOrdenacao = new TelaOrdenacao();

	public static AnchorPane getTelaMenu() {
		telaMenu = new AnchorPane();
		telaMenu.getChildren().add(elementos.getLblTituloOpcoes());
		telaMenu.getChildren().add(elementos.getLbTituloSemestre());
		telaMenu.getChildren().add(elementos.getBtnOpcaoFatorial(Interface.frame, telaFatorial.getSceneFatorial()));
		telaMenu.getChildren()
				.add(elementos.getBtnOpcaoOrdenarTexto(Interface.frame, telaOrdenacao.getScenOrdenacao()));
		return telaMenu;
	}

	public Scene getSceneMenu() {
		sceneMenu = new Scene(getTelaMenu(), 500, 400);
		sceneMenu.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		return sceneMenu;
	}

}
