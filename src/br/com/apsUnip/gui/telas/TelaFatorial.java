package br.com.apsUnip.gui.telas;

import br.com.apsUnip.gui.elementos.Elementos;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class TelaFatorial {

	private AnchorPane telaFatorial;
	private Scene sceneFatorial;

	private Elementos elementos = new Elementos();

	public AnchorPane getTelaFatorial() {
		telaFatorial = new AnchorPane();
		telaFatorial.getChildren().add(elementos.getLblTituloFatorial());
		telaFatorial.getChildren().add(elementos.getLbTituloSemestre());
		telaFatorial.getChildren().add(elementos.getBtnCalcularFatorial());
		telaFatorial.getChildren().add(elementos.getBtnLimparCampoFatorial());
		telaFatorial.getChildren().add(elementos.getTxtFatorial());
		telaFatorial.getChildren().add(elementos.getLblResultadoFatorial());
		return telaFatorial;
	}

	public Scene getSceneFatorial() {
		sceneFatorial = new Scene(getTelaFatorial(), 500, 400);
		sceneFatorial.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		return sceneFatorial;
	}

}
