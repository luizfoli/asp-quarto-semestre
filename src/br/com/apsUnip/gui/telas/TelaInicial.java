package br.com.apsUnip.gui.telas;

import br.com.apsUnip.gui.Interface;
import br.com.apsUnip.gui.elementos.Elementos;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class TelaInicial {

	private AnchorPane telaInicial;
	private Scene sceneInicia;

	private TelaMenu telaMenu = new TelaMenu();

	private Elementos elementos = new Elementos();

	public AnchorPane getTelaInicial() {
		telaInicial = new AnchorPane();
		telaInicial.getChildren().add(elementos.getBtnIniciar(Interface.frame, telaMenu.getSceneMenu()));
		telaInicial.getChildren().add(elementos.getBtnFechar());
		telaInicial.getChildren().add(elementos.getLbTituloCurso());
		telaInicial.getChildren().add(elementos.getLbTituloSemestre());
		return telaInicial;
	}

	public Scene getSceneInicial() {
		sceneInicia = new Scene(getTelaInicial(), 500, 400);
		sceneInicia.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		return sceneInicia;
	}
}
