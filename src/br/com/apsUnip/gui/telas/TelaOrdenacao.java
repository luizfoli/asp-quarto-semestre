package br.com.apsUnip.gui.telas;

import br.com.apsUnip.gui.elementos.Elementos;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class TelaOrdenacao {

	private AnchorPane telaOrdenacao;
	private Scene sceneOrdenacao;

	// private TelaInicial telaInicial = new TelaInicial();

	private Elementos elementos = new Elementos();

	public AnchorPane getTelaOrdenacao() {
		telaOrdenacao = new AnchorPane();
		telaOrdenacao.getChildren().add(elementos.getLblTituloOrdenacao());
		telaOrdenacao.getChildren().add(elementos.getLbTituloSemestre());
		telaOrdenacao.getChildren().add(elementos.getTxtEntradaOrdenacao());
		telaOrdenacao.getChildren().add(elementos.getTxtResultadoOrdenacao());
		telaOrdenacao.getChildren().add(elementos.getlblResultadoOrdenacao());
		telaOrdenacao.getChildren().add(elementos.getBtnOrdenarTexto());
		telaOrdenacao.getChildren().add(elementos.getBtnLimparCampoOrdenar());
		telaOrdenacao.getChildren().add(elementos.getLblTempoExecucaoOrdenacao());
		return telaOrdenacao;
	}

	public Scene getScenOrdenacao() {
		sceneOrdenacao = new Scene(getTelaOrdenacao(), 500, 400);
		sceneOrdenacao.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		return sceneOrdenacao;
	}
}
