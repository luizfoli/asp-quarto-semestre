package br.com.apsUnip.gui.elementos;

import br.com.apsUnip.gui.apiDev.CreateNodes;
import br.com.apsUnip.gui.elementos.var.VariaveisElementos;
import br.com.apsUnip.logica.Fatorial;
import br.com.apsUnip.logica.Ordenacao;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * Classe respons�vel por armazenar todos os bot�es que ser�o usados na
 * interface.
 * 
 * @author Luiz / Matheus
 *
 */

public class Elementos {

	private CreateNodes node = new CreateNodes();
	private VariaveisElementos var = new VariaveisElementos();

	// Labels

	private Label lbTituloCurso;
	private Label lbTituloSemestre;

	private Label lblTituloOpcoes;

	private Label lblTituloFatorial;
	private Label lblResultadoFatorial;

	private Label lblTituloOrdenacao;
	private Label lblResultadoOrdenacao;
	private Label lblTempoExecucaoOrdenacao;

	// Bot�es

	private Button btnIniciar;
	private Button btnFechar;
	private Button btnVoltarMenu;

	private Button btnOpcaoFatorial;
	private Button btnCalcularFatorial;
	private Button btnLimparCampoFatorial;

	private Button btnOpcaoOrdenarTexto;
	private Button btnOrdenarTexto;
	private Button btnLimparCampoOrdenar;

	// TextField // TextArea

	private TextField txtFatorial;

	private TextArea txtEntradaOrdenacao;
	private TextArea txtResultadoOrdenacao;

	public Label getLbTituloCurso() {
		lbTituloCurso = node.createLabel(var.getTextoLabelPrimeiroTituloMenu());
		lbTituloCurso.setLayoutX(35);
		lbTituloCurso.setLayoutY(20);
		lbTituloCurso.getStyleClass().add("TitleMenu");
		return lbTituloCurso;
	}

	public Label getLbTituloSemestre() {
		lbTituloSemestre = node.createLabel(var.getTextoLabelSubTituloMenu());
		lbTituloSemestre.setLayoutX(250);
		lbTituloSemestre.setLayoutY(70);
		lbTituloSemestre.getStyleClass().add("SubTitleMenu");
		return lbTituloSemestre;
	}

	public Label getLblTituloOpcoes() {
		lblTituloOpcoes = node.createLabel("Funcionalidades");
		lblTituloOpcoes.setLayoutX(100);
		lblTituloOpcoes.setLayoutY(20);
		lblTituloOpcoes.getStyleClass().add("TitleMenu");
		return lblTituloOpcoes;
	}

	public Label getLblTituloFatorial() {
		lblTituloFatorial = node.createLabel("Calculadora de Fatorial");
		lblTituloFatorial.setLayoutX(45);
		lblTituloFatorial.setLayoutY(20);
		lblTituloFatorial.getStyleClass().add("TitleMenu");
		return lblTituloFatorial;
	}

	public Label getLblResultadoFatorial() {
		lblResultadoFatorial = node.createLabel("Resultado: ");
		lblResultadoFatorial.setLayoutX(40);
		lblResultadoFatorial.setLayoutY(300);
		lblResultadoFatorial.getStyleClass().add("TitleMenu");
		return lblResultadoFatorial;
	}

	public Label getLblTituloOrdenacao() {
		lblTituloOrdenacao = node.createLabel("Ordenar Alfabeticamente");
		lblTituloOrdenacao.setLayoutX(20);
		lblTituloOrdenacao.setLayoutY(18);
		lblTituloOrdenacao.getStyleClass().add("TitleMenu");
		return lblTituloOrdenacao;
	}

	public Label getlblResultadoOrdenacao() {
		lblResultadoOrdenacao = node.createLabel("Resultado");
		lblResultadoOrdenacao.setLayoutX(270);
		lblResultadoOrdenacao.setLayoutY(130);
		lblResultadoOrdenacao.getStyleClass().add("TitleMenu");
		return lblResultadoOrdenacao;
	}

	public Label getLblTempoExecucaoOrdenacao() {
		lblTempoExecucaoOrdenacao = node.createLabel("Tempo Execu��o: ");
		lblTempoExecucaoOrdenacao.setLayoutX(270);
		lblTempoExecucaoOrdenacao.setLayoutY(350);
		lblTempoExecucaoOrdenacao.getStyleClass().add("SubSubTitleMenu");
		return lblTempoExecucaoOrdenacao;
	}

	public Button getBtnIniciar(Stage frame, Scene scene) {

		btnIniciar = node.createButton(var.getTextoBotaoIniciar(), 150, 200);
		btnIniciar.getStyleClass().add("BtnOption");

		btnIniciar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				frame.setScene(scene);
				frame.show();
			}
		});

		return btnIniciar;
	}

	public Button getBtnFechar() {
		btnFechar = node.createButton(var.getTextoBotaoFechar(), 145, 300);
		btnFechar.getStyleClass().add("BtnOption");

		btnFechar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				System.exit(0);
			}
		});

		return btnFechar;
	}

	public Button getBtnVoltarMenu(Stage frame, Scene scene) {
		btnVoltarMenu = node.createButton("Menu", 10, 20);
		btnVoltarMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				frame.setScene(scene);
				frame.show();
			}
		});
		return btnVoltarMenu;
	}

	public Button getBtnOpcaoFatorial(Stage frame, Scene scene) {
		btnOpcaoFatorial = node.createButton("Calcular Fatorial", 110, 220);
		btnOpcaoFatorial.getStyleClass().add("BtnOption");

		btnOpcaoFatorial.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				frame.setScene(scene);
				frame.show();
			}
		});

		return btnOpcaoFatorial;
	}

	public Button getBtnCalcularFatorial() {
		btnCalcularFatorial = node.createButton("Calcular", 115, 220);
		btnCalcularFatorial.getStyleClass().add("SubBtn");

		btnCalcularFatorial.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String numero = txtFatorial.getText();
				String fatorial = String.valueOf(Fatorial.calcula(Integer.parseInt(numero)));

				lblResultadoFatorial.setText("Resultado: " + fatorial);
			}
		});

		return btnCalcularFatorial;
	}

	public Button getBtnLimparCampoFatorial() {
		btnLimparCampoFatorial = node.createButton("Limpar", 275, 220);
		btnLimparCampoFatorial.getStyleClass().add("SubBtn");

		btnLimparCampoFatorial.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				txtFatorial.setText("");
				lblResultadoFatorial.setText("Resultado: ");
			}
		});

		return btnLimparCampoFatorial;
	}

	public Button getBtnOpcaoOrdenarTexto(Stage frame, Scene scene) {
		btnOpcaoOrdenarTexto = node.createButton("Ordernar Textos", 110, 300);
		btnOpcaoOrdenarTexto.getStyleClass().add("BtnOption");

		btnOpcaoOrdenarTexto.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				frame.setScene(scene);
				frame.show();
			}
		});

		return btnOpcaoOrdenarTexto;
	}

	public Button getBtnOrdenarTexto() {
		btnOrdenarTexto = node.createButton(var.getTextoBotaoOrdenar(), 40, 345);
		btnOrdenarTexto.getStyleClass().add("SubSubBtn");

		btnOrdenarTexto.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				String textoEntrada = txtEntradaOrdenacao.getText();
				String resultadoOrdenacao = Ordenacao.ordena(textoEntrada);

				long inicioTempo = Ordenacao.iniciaContarTempo();

				txtResultadoOrdenacao.setText(resultadoOrdenacao);

				long fimTempo = Ordenacao.finalizaContarTempo();

				long fim = Ordenacao.tempoExecucao(inicioTempo, fimTempo);

				lblTempoExecucaoOrdenacao.setText("Tempo Execu��o (Nano): " + String.valueOf(fim));
			}

		});

		return btnOrdenarTexto;
	}

	public Button getBtnLimparCampoOrdenar() {
		btnLimparCampoOrdenar = node.createButton(var.getTextoBotaoLimpar(), 143, 345);
		btnLimparCampoOrdenar.getStyleClass().add("SubSubBtn");

		btnLimparCampoOrdenar.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				txtEntradaOrdenacao.setText("");
				txtResultadoOrdenacao.setText("");
			}

		});

		return btnLimparCampoOrdenar;
	}

	public TextField getTxtFatorial() {
		txtFatorial = node.createTextField(120, 150);
		txtFatorial.getStyleClass().add("Txt");
		txtFatorial.setPrefWidth(280);
		txtFatorial.setPrefHeight(50);
		txtFatorial.setFont(Font.font("Verdana", FontWeight.BOLD, 30));

		txtFatorial.textProperty().addListener(new InvalidationListener() {

			@Override
			public void invalidated(Observable observable) {
				if (!txtFatorial.getText().matches("\\d*")) {
					txtFatorial.setText(txtFatorial.getText().replaceAll("[^\\d]", ""));
				}
			}
		});

		return txtFatorial;
	}

	public TextArea getTxtEntradaOrdenacao() {
		txtEntradaOrdenacao = node.createTextArea();
		txtEntradaOrdenacao.setLayoutX(40);
		txtEntradaOrdenacao.setLayoutY(180);
		txtEntradaOrdenacao.setPrefSize(200, 150);
		return txtEntradaOrdenacao;
	}

	public TextArea getTxtResultadoOrdenacao() {
		txtResultadoOrdenacao = node.createTextArea();
		txtResultadoOrdenacao.setLayoutX(270);
		txtResultadoOrdenacao.setLayoutY(180);
		txtResultadoOrdenacao.setPrefSize(200, 150);
		txtResultadoOrdenacao.setEditable(false);
		return txtResultadoOrdenacao;
	}
}
