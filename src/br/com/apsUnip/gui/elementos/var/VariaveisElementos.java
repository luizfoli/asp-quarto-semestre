package br.com.apsUnip.gui.elementos.var;

/**
 * Classe respons�vel por armezar todas as variaveis que ser�o, usadas nos
 * elementos da interface.
 * 
 * @author Luiz / Matheus
 *
 */

public class VariaveisElementos {

	private final String TEXTO_BOTAO_INICIAR = "Iniciar";
	private final String TEXTO_BOTAO_FECHAR = "Fechar";
	private final String TEXTO_BOTAO_ORDENAR = "Ordenar";
	private final String TEXTO_BOTAO_LIMPAR = "Limpar";

	private final String TEXTO_LABEL_PRIMEIRO_TITULO_MENU = "Ci�ncia da Computa��o";
	private final String TEXTO_LABEL_SUB_TITULO_MENU = "APS - 4� Semestre";

	public String getTextoLabelSubTituloMenu() {
		return TEXTO_LABEL_SUB_TITULO_MENU;
	}

	public String getTextoLabelPrimeiroTituloMenu() {
		return TEXTO_LABEL_PRIMEIRO_TITULO_MENU;
	}

	public String getTextoBotaoIniciar() {
		return TEXTO_BOTAO_INICIAR;
	}

	public String getTextoBotaoFechar() {
		return TEXTO_BOTAO_FECHAR;
	}

	public String getTextoBotaoOrdenar() {
		return TEXTO_BOTAO_ORDENAR;
	}

	public String getTextoBotaoLimpar() {
		return TEXTO_BOTAO_LIMPAR;
	}
}
