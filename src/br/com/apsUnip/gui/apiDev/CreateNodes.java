package br.com.apsUnip.gui.apiDev;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * 
 * @author Luiz Felipe Oliveira
 *
 *         Classe respons�vel pelo metodos que criam os objetos na tela, todos
 *         s�o do tipo Node.
 *
 */

public class CreateNodes {

	public ImageView createImageView(String nome_img) {
		ImageView img = new ImageView(nome_img);
		return img;
	}

	public ImageView createImageView(String nome_img, int x, int y) {
		ImageView img = new ImageView(nome_img);
		img.setTranslateX(x);
		img.setTranslateY(y);
		img.setCursor(Cursor.HAND);
		return img;
	}

	public Circle createCircle(int x, int y) {
		Circle cir = new Circle(80.0f, Color.GREEN);
		cir.setCenterX(x);
		cir.setCenterY(y);
		cir.setVisible(false);
		return cir;
	}

	public Button createButton(String texto, int x, int y) {
		Button btn = new Button(texto);
		btn.setTranslateX(x);
		btn.setTranslateY(y);
		return btn;
	}

	public Label createLabel(String texto) {
		Label lb = new Label(texto);
		return lb;
	}

	public TextField createTextField() {
		TextField txt = new TextField();
		return txt;
	}
	
	public TextField createTextField(int x, int y) {
		TextField txt = new TextField();
		txt.setTranslateX(x);
		txt.setTranslateY(y);
		return txt;
	}

	public TextArea createTextArea() {
		TextArea txt = new TextArea();
		txt.setStyle("-fx-font-size: 14");
		txt.setWrapText(true);
		return txt;
	}
}
